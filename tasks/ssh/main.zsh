#!/usr/bin/zsh

TASK_FOLDER="$GIT_TARGET"/tasks/ssh/bin


#####################################################
task_title="ssh subtask 1 - generate key pair" 
print_task_title "$task_title"
. "$TASK_FOLDER"/'generate-keys.zsh'

#####################################################
task_title="ssh subtask 2 - create alias to gen ssh host fingerprints"
print_task_title "$task_title"
. "$TASK_FOLDER"/'host-fingerprint.zsh'

