#!/usr/bin/zsh

set -e

keytype='rsa'
keylength='4096'

prefix='key_'"$keytype"'_'"$keylength"

if [ -f '/usr/bin/hostname' ]
then
	hostname="$( /usr/bin/hostname -s )"
else
	hostname="$( /bin/hostname -s )"
fi

username="$( /usr/bin/whoami )"

if [ -f '/usr/bin/date' ]
then
	timestamp="$( /usr/bin/date +"%F--%H-%M-%S" )"	
else
	timestamp="$( /bin/date +"%F--%H-%M-%S" )"	
fi

	foldername="$hostname"''-"$timestamp"
	mkdir "$foldername"

	filename="$prefix"'_'"$hostname"'_'"$username"

# -N : new password, make it blank here
/usr/bin/ssh-keygen \
	-t "$keytype" \
	-b "$keylength" \
	-f "$filename" \
	-N ''

key_private="$filename"'.private'
key_public="$filename"'.public'

	mv "$filename" "$key_private"
	mv "$filename"'.pub' "$key_public"

  # make symlinks
	ln -s "$filename"'.private' id_rsa
  ln -s "$key_private"        key_private
  ln -s "$key_public"         key_public

  # renew the symlink if it exists
  rm -f keys.d
  ln -s "$foldername"         keys.d

# group all of them into a single folder
# files
mv ./"$key_private" ./"$foldername"
mv ./"$key_public"  ./"$foldername"

# symlinks
mv ./id_rsa         ./"$foldername"
mv ./'key_public'   ./"$foldername"
mv ./'key_private'  ./"$foldername"

