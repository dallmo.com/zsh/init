
# bin
- scripts to perform the config on zsh-related configs

# lib
- existing scripts that perform certain jobs on it's own ( e.g. generate ssh host keys ) to be copied onto the hosts, or linked via alias

