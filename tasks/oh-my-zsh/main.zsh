#!/usr/bin/zsh

TASK_NAME="oh-my-zsh"
TASK_BIN_FOLDER="$GIT_TARGET"/tasks/"$TASK_NAME"/bin


#####################################################
# add custom alias
. "$TASK_BIN_FOLDER"/'add-alias.zsh'

