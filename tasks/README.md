
# overview
subtasks are all grouped under this folder, 
either called via the "start.zsh" script in the parent folder,
or called individually on-deman here.

---

every task starts from the file `main.zsh`

# bin
- scripts to perform the config on zsh-related configs

# lib
- existing scripts that perform certain jobs on it's own ( e.g. generate ssh host keys ) to be copied onto the hosts, or linked via alias

