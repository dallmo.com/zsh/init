#!/usr/bin/zsh

PARENT_TASK="ssh" # this must be the same as the folder
TASK_DESC="add host fingerprint script" # about the current task

##########################################################
# common among all task bin scripts
#TASK_FOLDER="$GIT_TARGET"/tasks/"$PARENT_TASK"
#TASK_TITLE="$PARENT_TASK - $TASK_DESC"
#print_task_title "$TASK_TITLE"
. "$INCLUDE_FOLDER"/'subtask.zsh'

##########################################################
# the task script to include
script_file="$TASK_FOLDER"/lib/'ssh.host.fingerprint.zsh'

  echo "this file is to be added for host fingerprint generation : "
  echo "$divider"
  head "$script_file"

