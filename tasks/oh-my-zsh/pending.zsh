#!/usr/bin/zsh

TASK_NAME="oh-my-zsh"
TASK_BIN_FOLDER="$GIT_TARGET"/tasks/"$TASK_NAME"/bin


#####################################################
# install oh-my-zsh
task_title="oh-my-zsh subtask 1 - install" 
print_task_title "$task_title"
. "$TASK_BIN_FOLDER"/'install-ohmyzsh.zsh'

#####################################################
# config update mode and interval
#   zstyle ':omz:update' mode auto      # update automatically without asking
#   zstyle ':omz:update' frequency 1
task_title="oh-my-zsh subtask 2 - adjust update"
print_task_title "$task_title"
. "$TASK_BIN_FOLDER"/'adjust-update.zsh'

#####################################################
# install oh-my-zsh theme : headline
task_title="oh-my-zsh subtask 3 - install theme headline"
print_task_title "$task_title"
. "$TASK_BIN_FOLDER"/'install-theme.zsh'

#####################################################
# add custom alias
task_title="oh-my-zsh subtask 4 - add custom alias"
print_task_title "$task_title"
. "$TASK_BIN_FOLDER"/'add-alias.zsh'

