#!/bin/bash

# this is the entry point of all scripts
# which prepares the git repo for referencing required by all scripts

#####################################################################
# ref : https://stackoverflow.com/questions/19622198/what-does-set-e-mean-in-a-bash-script
set -e

#####################################################################
# check if git exists
echo "check if git exists ..."
exit_state="$( git --version > /dev/null 2>&1; echo $? )"

# if there is any error, install git
if [ "$exit_state" -ne 0 ]
then
  echo "install git now."
  sudo apt -y install git
else
  echo "git already installed."
fi

echo "git is now ready, so move on..."

#####################################################################
# check if system root is writable, 
# and determine the location of storeroom accordingly
if [ -w / ]
then
  echo "root is writable."
  STOREROOM="/storeroom"
else
  echo "root is not writable."
  STOREROOM="$HOME/storeroom"
fi

#####################################################################
# define this only after STOREROOM is defined
GIT_SOURCE="https://gitlab.com/dallmo.com/zsh/init.git"
GIT_TARGET_FOLDER="$STOREROOM/gitlab/dallmo.com/zsh"
GIT_TARGET="$GIT_TARGET_FOLDER"/'init.clone'
INCLUDE_FOLDER="$GIT_TARGET"/'include'
TASK_FOLDER="$GIT_TARGET"/'tasks'

#####################################################################
# clone git source, and prepare folders if needed
# check if git target directory exists
if [ -d "$GIT_TARGET_FOLDER" ]
then
  echo "git target : $GIT_TARGET_FOLDER already exists.  move on."
else
  echo "creating git target : $GIT_TARGET_FOLDER"
  mkdir -p "$GIT_TARGET_FOLDER"
fi

# check if the clone directory is there
if [ -d "$GIT_TARGET" ]
then
  echo "git cloned before, check for updates now ..."
  cd "$GIT_TARGET"; git pull;
else
  echo "do git clone now:"
  git clone "$GIT_SOURCE" "$GIT_TARGET"
fi

echo "git clone updated."

#####################################################################
# include vars / util functions here
. "$INCLUDE_FOLDER"/'vars.conf'
. "$INCLUDE_FOLDER"/'func.conf'

#####################################################################
# this contains all tasks
. "$TASK_FOLDER"/'tasks_list.zsh'

