#!/usr/bin/zsh

PARENT_TASK="oh-my-zsh" # this must be the same as the folder
TASK_DESC="add alias to custom folder" # about the current task

##########################################################
# common among all task bin scripts
. "$INCLUDE_FOLDER"/'subtask.zsh'

##########################################################
# the task script to include
alias_file="$TASK_LIB_FOLDER"/'alias.zsh'

  echo "this file is to be added to oh-my-zsh custom folder : "
  echo "$divider"
    cat "$alias_file"
  echo "$divider"
  echo "oh-my-zsh custom folder : $ZSH_CUSTOM"
  echo "$divider"
