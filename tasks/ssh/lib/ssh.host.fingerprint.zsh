#!/usr/bin/zsh

# host var, = 0 false, = 1 true
host_ubuntu=0
host_darwin=0

# determine the host
check_host_ubuntu="$( uname -a | grep -i ubuntu )"
	exit_state_ubuntu=$?

check_host_darwin="$( uname -a | grep -i darwin )"
	exit_state_darwin=$?

	#----------------------------------------------------------
	# check if the host is ubuntu
	if [ $exit_state_ubuntu -eq 0 ]
	then
		echo "$HOSTNAME is ubuntu."
		host_ubuntu=1
	fi

	#----------------------------------------------------------
	# check if the host is darwin, i.e. mac os x
	if [ $exit_state_darwin -eq 0 ]
	then
		echo "$HOSTNAME is darwin."
		host_darwin=1
	fi

# source : 
# http://superuser.com/questions/929566/sha256-ssh-fingerprint-given-by-the-client-but-only-md5-fingerprint-known-for-se

# standard sshd config path
SSHD_CONFIG=/etc/ssh/sshd_config

# helper functions
function tablize { 
        awk '{printf("| %-7s | %-7s | %-52s |\n", $1, $2, $3)}'
}
LINE="+---------+---------+------------------------------------------------------+"

# header
echo $LINE
echo "Cipher" "Algo" "Fingerprint" | tablize
echo $LINE

# fingerprints
host_key_list="$( ls /etc/ssh/ssh_host_*.pub )"

echo "$host_key_list" |\
	while read host_key
	do
		cipher=$(echo $host_key | sed -r 's/^.*ssh_host_([^_]+)_key\.pub$/\1/'| tr '[a-z]' '[A-Z]')
		if [[ -f "$host_key" ]]; then

			md5=$(ssh-keygen -l -f $host_key | awk '{print $2}')
			echo $cipher MD5 $md5 | tablize

			#if [ $host_ubuntu -eq 1 ]
			#then
				md5_2=$(ssh-keygen -E md5 -lf $host_key | awk '{print $2}')
				echo $cipher 'MD5,2' $md5_2 | tablize
			#fi

      # for sha256sum
      if [ -f /usr/bin/sha256sum ]
      then
			  sha256=$(awk '{print $2}' $host_key | base64 -d | sha256sum -b | awk '{print $1}' | xxd -r -p | base64)
      else 
        # for shasum
        if [ -f /usr/bin/shasum ]
        then
          sha256=$(awk '{print $2}' $host_key | base64 -d | shasum -b | awk '{print $1}' | xxd -r -p | base64)
        fi # for shasum
      fi # for sha256sum

      # print the cipher
			echo $cipher SHA-256 $sha256 | tablize
			echo $LINE
		fi
	done

