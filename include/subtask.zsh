
##########################################################
# common among all task bin scripts
PARENT_TASK_FOLDER="$GIT_TARGET"/tasks/"$PARENT_TASK"
TASK_LIB_FOLDER="$PARENT_TASK_FOLDER"/'lib'
TASK_TITLE="$PARENT_TASK - $TASK_DESC"

# print subtask info
print_task_title "$TASK_TITLE"

