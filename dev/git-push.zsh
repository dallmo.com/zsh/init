#!/usr/bin/zsh

comment="$1"

cd ..

git pull
git add .
git commit -am "$comment"
git push

cd -

