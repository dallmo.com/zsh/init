# overview

initial common setup of zsh on a new machine

# quick start / update
to add/remove any task, edit the file :
```
tasks/tasks_list.zsh
```


# included config

- oh-my-zsh
  - official installation guide : https://ohmyz.sh/#install

- oh-my-zsh theme - headline
  - official installation guide : https://github.com/Moarram/headline/wiki/Installation

- alias
  - a simple file to be included inside the custom folder of oh-my-zsh
    - `$ZSH_CUSTOM`


# architecture
- start.zsh
  - start with preparing the git repo for referencing, then add tasks

# dev
- `dev/build.zsh`
  - when there is new changes made to the scripts

- `dev/test.zsh`
  - run test via `curl` only
