# 2022-04-02
# jimz@dallmo
# custom alias to be added inside ~/.oh-my-zsh/custom
alias l='ls -l'
alias c='clear'
alias cl='c; l'

